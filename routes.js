const express = require('express')
const cors =  require('cors')
const router = express.Router()
const dados = require('./dados.json')
var app = express()

app.use(cors())

router.get('/paises/aleatorio', function(req, res){ ///Segundo Get: gera objeto aleatorio//
    let info = aleatorio()
    res.json(info)
})


router.get('/paises/:atributo/:valor', function(req, res){  //Primeiro Get     
let list = []
let listNoLimit = []
let limite = req.query.limite || 5
let atr = req.params.atributo 
let val = req.params.valor

    if(atr in dados)
    {        
       for(let i=0; i<dados.ID.length; i++)
       {        
            if(dados[atr][i] == val){
                const info = {
                    Pais: dados.pais[i],
                    Capital: dados.capital[i],
                    Presidente: dados.presidente[i],
                    Lingua: dados.lingua[i],
                    ID: dados.ID[i]
                }
                if(list.length < limite)
                {
                    list.push(info)                    
                } 
                else
                {
                    listNoLimit.push(info)
                }                          
            }        
       }   
        if(limite)
            res.json(list)
        else   
            res.json(listNoLimit)
            
    }
    else{
        console.log("NAO ENTROU NO IF")
    }    
})


router.get('/paises/capitais', function(req, res){    //Terceiro get:retorna as capitais 
    res.json(dados.capital)
})


function aleatorio(){
    const i = Math.floor(Math.random()* 5)
    const info = {
        Pais: dados.pais[i],
        Capital: dados.capital[i],
        Presidente: dados.presidente[i],
        Lingua: dados.lingua[i],
        ID: dados.ID[i]
    }
    return info
} 
module.exports = router