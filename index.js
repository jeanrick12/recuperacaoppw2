const express = require('express')
const app = express()
const PORTA = process.env.PORT || 3000
const routes = require('./routes')

app.use('/api' , routes)

app.listen(PORTA, function(){

    console.log(`Servidor rodando ${PORTA}`)
})